package pars

// ParsingError indicates that an error occurred during parsing
type ParsingError struct {
	msg string
}

func (e *ParsingError) Error() string {
	return e.msg
}

// NewParsingError creates a pointer to a ParsingError with the given message
func NewParsingError(msg string) *ParsingError {
	return &ParsingError{msg}
}

// CancelledError indicates that the scanning was cancelled before completing
type CancelledError struct {
	msg string
}

func (e *CancelledError) Error() string {
	return e.msg
}

// NewCancelledError creates a pointer to a CancelledError with the given message
func NewCancelledError(msg string) *CancelledError {
	return &CancelledError{msg}
}
