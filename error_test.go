package pars

import "testing"

func TestParsingError(t *testing.T) {
	msg := "context cancelled"
	err := NewParsingError(msg)
	if nil == err {
		t.Error("the error should not be nil")
	}
	if msg != err.Error() {
		t.Errorf("error message should be '%s' not '%s'", msg, err.Error())
	}
}
