pars
===
pars is a simple abstraction for implementing a generic, single function parsing interface, that takes in a source string and returns a parsed object.

Getting Started
---
A Parser interface has the function Parse. Parse takes in a string containing the source to parse and returns an element of the empty interface and an error.

### How It Works
There is a [Parser](./parser.go) interface with a single function, Parse.

### In the Wild
-   An example that currently uses pars's Parser interface is the [scrapegoat](https://bitbucket.org/athrun22/scrapegoat/src/develop/) project.
-   An example that has a Parser implementation is [sirius](https://bitbucket.org/athrun22/sirius/src/develop/) which scrapes dynamically populated currently playing song names and artists from a SiriusXM channel.

Versioning
---
[SemVer](http://semver.org/) is used for versioning. For the versions available, see the tags on this repository.

Contributing
---
Please read [CONTRIBUTING.md](./CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

Authors
---
-   Jordan Williams

License
---
This project is licensed under the MIT License - see the [LICENSE.md](./LICENSE.md) file for details
