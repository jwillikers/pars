package pars

// Parser parses a given string, extracting objects of interest and returning an error if pertinent
type Parser interface {
	Parse(*string) (interface{},
		error)
}
